<?php

namespace App\Other;

use App\Utility\Utility;

/**
 * Description of Other
 *
 * @author Mazhar
 */
class Other {

    public $id = "";
    public $users_id = "";
    public $languages = "";
    public $reference = "";

    public function __construct($data = false) {

        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("resume") or die("Cannot Select database");


        if (is_array($data) && array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        $this->users_id = $data['users_id'];
        $this->languages = $data['languages'];
        $this->reference = $data['reference'];
    }

    public function search() {
        $phone = array();
        if (isset($_POST['submit'])) {
            if (isset($_GET['go'])) {
                if (preg_match("/^[  a-zA-Z]+/", $_POST['name'])) {
                    $name = $_POST['name'];
//                    $db = mysql_connect("localhost", "root", "") or die('I cannot connect to the database  because: ' . mysql_error());
//                    $mydb = mysql_select_db("phonebook");
                    $sql = "SELECT  languages, reference FROM others WHERE reference LIKE '%" . $name . "%'";
                    $result = mysql_query($sql);
                    while ($row = mysql_fetch_array($result)) {
                        $phone[] = $row;
                    }
                } else {
                    Utility::message("Please Search With Name!!");
                }
                return $phone;
            }
        }
    }

    public function show($id = false) {



        $query = "SELECT * FROM `others` WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function index() {


        $query = "SELECT * FROM `others`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {
            $books[] = $row;
        }
        return $books;
    }

    public function store() {
        $planguage = $_POST['languages'];
        $pl = strip_tags(implode(',', $planguage));
        $plang = rtrim($pl, ',');
        $user_id = $_SESSION['user_session'];

        $query = "INSERT INTO `resume`.`others` (`users_id`, `languages`, `reference`) VALUES ('" . $user_id . "', '" . $plang . "', '" . $this->reference . "');";
        $result = mysql_query($query);
        if ($result) {
            Utility::message("Details successfully inserted");
        } else {
            Utility::message("Details not inserted, try again later");
        }
        Utility::redirect('index.php');
    }

    public function update() {

        $planguage = $_POST['languages'];
        $pl = strip_tags(implode(',', $planguage));
        $plang = rtrim($pl, ',');
        $query = "UPDATE `resume`.`others` SET `languages` = '" . $plang . " ', `reference` = '" . $this->reference . "' WHERE `others`.`id` = " . $this->id;

        $result = mysql_query($query);
        if ($result) {
            Utility::message("Details successfully Edited");
        } else {
            Utility::message("Details not updated, try again later");
        }
        Utility::redirect('index.php');
    }

    public function delete($id = null) {
        if (is_null($id)) {
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }


        $query = "DELETE FROM `resume`.`others` WHERE `others`.`id` = " . $id;

        $result = mysql_query($query);
        if ($result) {
            Utility::message("Details is successfully Deleted");
        } else {
            Utility::message("Details can not delete");
        }
        Utility::redirect('index.php');
    }

}
